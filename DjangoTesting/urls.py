from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from main import views
from main import login_views
from main import register_views
from main import all_tables_views
from main import tables_views
from main import users_views

urlpatterns = [
  url(r'^admin/', admin.site.urls),
  path("", views.Home.as_view()),
  path('all_tables.html', all_tables_views.TableList.as_view(), name="all_tables"),
  path('User.html', users_views.addUser, name="Admin_Form"),
  path('register.html', register_views.register, name="Register_Form"),
  url(r'^table.html/', tables_views.Table.as_view(), name="table"),
  # url(r'^table.html/(?P<id>[0-9]+)/$', tables_views.Table.as_view(), name="table"),
  # <a href="{% url 'table' t.id %}"{{ t.name }}</a>
  url(r'login.html', login_views.Login.as_view(), name="login"),
]
