import unittest
from main.classes import Deck, Card
import unittest.mock

suite = unittest.TestSuite()


# Card tests
class CardInitCorrectRankAndSuit(unittest.TestCase):

    def testInRank(self):
        for i in range(1, 14):
            rankcard = Card.Card(1, i)
            self.assertEquals(rankcard.__getattribute__("rank"), i)

    def testInSuit(self):
        for i in range(1, 5):
            suitcard = Card.Card(i, 2)
            self.assertEquals(suitcard.__getattribute__("suit"), i)

    pass


suite.addTest(unittest.makeSuite(CardInitCorrectRankAndSuit))


class CardInitRaiseValueError(unittest.TestCase):

    def testNotInRank(self):
        self.assertRaises(ValueError, a=Card.Card(1, 0))
        self.assertRaises(ValueError, a=Card.Card(1, 14))

    def testNotInSuit(self):
        self.assertRaises(ValueError, a=Card.Card(0, 1))
        self.assertRaises(ValueError, a=Card.Card(5, 2))

    pass


suite.addTest(unittest.makeSuite(CardInitRaiseValueError))

card0 = Card.Card(1, 1)
card1 = Card.Card(1, 9)
card2 = Card.Card(2, 11)
card3 = Card.Card(3, 12)
card4 = Card.Card(4, 13)


class CardGetSuite(unittest.TestCase):
    def test_getSuite_in_range(self):
        self.assertEqual(card1.getSuit(), 1)
        self.assertEqual(card2.getSuit(), 2)
        self.assertEqual(card3.getSuit(), 3)
        self.assertEqual(card4.getSuit(), 4)

    pass


suite.addTest(unittest.makeSuite(CardGetSuite))


class CardGetRank(unittest.TestCase):
    def test_getRank_in_range(self):
        self.assertEqual(card1.getRank(), 9)
        self.assertEqual(card2.getRank(), 11)
        self.assertEqual(card3.getRank(), 12)
        self.assertEqual(card4.getRank(), 13)

    pass


suite.addTest(unittest.makeSuite(CardGetRank))


class CardGetValue(unittest.TestCase):
    def test_get_value1(self):
        self.assertEqual(card0.getValue(), 14)
        self.assertEqual(card1.getValue(), 9)
        self.assertEqual(card2.getValue(), 11)
        self.assertEqual(card3.getValue(), 12)
        self.assertEqual(card4.getValue(), 13)
        # ACE has value 14, bigger than other cards

    def test_get_value2(self):
        self.assertFalse(card0.getValue() < card1.getValue())
        self.assertTrue(card1.getValue() < card2.getValue())
        self.assertTrue(card2.getValue() < card3.getValue())
        self.assertTrue(card3.getValue() < card4.getValue())
        self.assertTrue(card4.getValue() < card0.getValue())

    pass


suite.addTest(unittest.makeSuite(CardGetValue))


class CardStr(unittest.TestCase):

    def test_str(self):
        s = ""
        for i in range(1, 14):  # rank of card
            for x in range(1, 5):  # suit of card
                a = Card.Card(x, i)  # create card
                if i == 1:
                    s += "A"
                elif i == 11:
                    s += "J"
                elif i == 12:
                    s += "Q"
                elif i == 13:
                    s += "K"
                else:
                    s += str(i)
                if x == 1:
                    s += "C"
                elif x == 2:
                    s += "S"
                elif x == 3:
                    s += "H"
                else:
                    s += "D"
                self.assertEquals(s, a.__str__())

    pass


suite.addTest(unittest.makeSuite(CardStr))


# deck tests

# you must add these tests to the suite

class DeckInit(unittest.TestCase):
    def test_deck_init(self):
        test_deck = Deck.Deck()
        self.assertEquals(test_deck.__getattribute__("num_cards"))
        for i in range(52):
            rank = 1
            suit = 1
            self.assertEquals(test_deck.card_array[i.getRank], rank)
            self.assertEquals(test_deck.card_array[i.getSuit], suit)
            rank += rank
            if rank == 14:
                rank = 1
                suit += suit
    pass


suite.addTest(unittest.makeSuite(DeckInit))


class DeckCount(unittest.TestCase):
    def test_max_deck(self):  # Test for 52 card deck
        test_deck = Deck.Deck()
        self.assertEqual(test_deck.count(), 52)

    def test_reg_deck(self):  # Test for a deck with missing cards
        test_deck = Deck.Deck()
        for i in range(10):
            test_deck.draw()
        self.assertEqual(test_deck.count(), 42)

    def test_minDeck(self):  # Test for a deck with no cards
        test_deck = Deck.Deck()
        for i in range(52):
            test_deck.draw()
        self.assertEqual(test_deck.count(), 0)

    pass


suite.addTest(unittest.makeSuite(DeckCount))


class DeckDrawValue(unittest.TestCase):
    def deck(self):
        decktester = Deck.Deck()
        self.assertEquals(decktester.draw(), decktester.cardarray[51])
        self.assertEquals(decktester.count(), 51)

    pass


suite.addTest(unittest.makeSuite(DeckDrawValue))


class DeckDrawException(unittest.TestCase):
    def deck(self):
        decktester = Deck.Deck()
        decktester.num_cards = 0

        self.assertRaises(ValueError, decktester.draw(), 0)

    pass


suite.addTest(unittest.makeSuite(DeckDrawException))


class DeckShuffle(unittest.TestCase):
    def test_shuffle(self):
        deck_tester = Deck.Deck()
        cards_hash = hash(deck_tester.lscards)
        deck_tester.shuffle()
        self.assertNotEqual(hash(deck_tester.lscards), cards_hash)
    pass


suite.addTest(unittest.makeSuite(DeckShuffle))