from django.test import TestCase
from main.apps import MyApp


# user story:
# As a player, I can see the 5 highest scores since the game has been reset
# Acceptance Criteria:
# As a player, I can see the 5 highest scores since the game has been reset
# by typing get_highscore, it should return max 5 highscores of top 5 players.
from main.classes import table
from main.models import Person


class TestHighScore(TestCase):
    def test_init(self):
        a = MyApp()
        response = a.doStuff("user1:get highscore")
        self.assertEqual(response, "1st:0, 2nd:0, 3rd:0, 4th:0, 5th:0")

    def test_get_highscore(self):
        t = table.Table("table_A")
        user1 = Person.objects.create(name="user1", highscore=1000)
        user2 = Person.objects.create(name="user2", highscore=2000)
        user3 = Person.objects.create(name="user3", highscore=3000)
        t.add_player(user1)
        t.add_player(user2)
        t.add_player(user3)
        a = MyApp()
        response = a.doStuff("user1:get highscore")
        self.assertEqual(response, "1st:0=3000, 2nd:2000, 3rd:1000, 4th:0, 5th:0")

        user4 = Person.objects.create(name="user4", highscore=1000)
        user5 = Person.objects.create(name="user5", highscore=2000)
        user6 = Person.objects.create(name="user6", highscore=3000)
        t.add_player(user4)
        t.add_player(user5)
        t.add_player(user6)
        response = a.doStuff("user1:get highscore")
        self.assertEqual(response, "1st:6000, 2nd:5000, 3rd:4000, 4th:3000, 5th:2000")