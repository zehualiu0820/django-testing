from django.test import TestCase
from main.apps import MyApp
from main.classes import Card
from main.classes import table
from main.models import Person


# user story:
# As a player, I can decide whether to call, check, raise, or fold on my turn
# Acceptance Criteria:
# Each of my commands works correctly, player can choose only one command once.

class command(TestCase):
    def check_command(self):
        a = MyApp()
        t = table.Table("table1")
        user1 = Person.objects.create(name="user1", highscore=1000)
        user2 = Person.objects.create(name="user2", highscore=2000)
        user3 = Person.objects.create(name="user3", highscore=3000)
        t.pot_size = 0
        t.add_player(user1)
        t.add_player(user2)
        t.add_player(user3)
        a.start_game()
        a.doStuff("user1:check")
        self.assertEqual(t.pot_size, "0")
        a.doStuff("user2:call")
        self.assertEqual(t.pot_size, "100")
        a.doStuff("user3:call")
        self.assertEqual(t.pot_size, "200")
        response= a.doStuff("user2:call")
        self.assertEqual(response, "It's not your turn!")
        a.doStuff("user1:raise 200")
        self.assertEqual(t.pot_size, "400")
        a.doStuff("user2:call")
        self.assertEqual(t.pot_size, "600")
        a.doStuff("user3:fold")
        self.assertEqual(t.pot_size, "600")
        a.doStuff("user1:call")
        self.assertEqual(t.pot_size, "800")
        a.doStuff("user2:fold")
        self.assertEqual(user1.highscore, "1800")