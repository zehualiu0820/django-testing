from django.test import TestCase
from main.apps import MyApp


# user story:
# as an admin only I have the power to create new users.
# Acceptance Criteria:
# only an admin can create a user, admin can not create an admin
# a duplicate user can not be created,
# username can only contain letters and numbers, no spaces allowed

# ADMIN ACCOUNTS ARE DENOTED BY AN ASTERISK (*) AT THE START OF THE USERNAME

class CreateUser(TestCase):
    def user_create_user(self):
        a = MyApp()
        response = a.doStuff("user:createUser user2")
        self.assertEqual(response, "user can not create another user")

    def admin_create_incorrect_user(self):
        a = MyApp()
        response = a.doStuff("*admin_user:createUser user#")
        self.assertEqual(response, "error: username can only contain letters and numbers")
        response = a.doStuff("*admin_user:createUser user 2")
        self.assertEqual(response, "error: username cannot have spaces")

    def admin_create_user(self):
        a = MyApp()
        response = a.doStuff("*admin_user:createUser user2")
        self.assertEqual(response, "user2 created")

    def admin_create_admin(self):
        a = MyApp()
        response = a.doStuff("*admin_user:createUser *admin_user2")
        self.assertEqual(response, "error! user can not be admin")

    def create_duplicate(self):
        a = MyApp()
        a.doStuff("*admin_user:createUser user2")
        response = a.doStuff("*admin_user:createUser user2")
        self.assertEqual(response, "error user2 already exists")
