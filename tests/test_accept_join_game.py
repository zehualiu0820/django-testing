from django.test import TestCase
from unittest import mock
from main.apps import MyApp


class JoinGame(TestCase):

    def join_full_game(self):
        a = MyApp()
        table.Table.__init__()
        table.size = 5
        table.name = "b"
        response = a.doStuff("user:join b")
        self.assertEquals(response, "table is full")

    def join_open_table(self):
        a = MyApp()
        table.Table.__init__()
        table.size = 4
        table.name = "b"
        response = a.doStuff("user:join b")
        self.assertEquals(response, "table joined")

    def non_existing_table(self):
        a = MyApp()
        response = a.doStuff("user:join b")
        self.assertEquals(response, "table does not exist")
