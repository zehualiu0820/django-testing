from django.test import TestCase
from unittest import mock
from main.apps import MyApp


# dingle bop!

class TestLobby(TestCase):

    def test_user_has_asterisk(self):
        meApp = MyApp()
        meApp.command = mock.Mock()
        inputCommand = meApp.command("admin:create lobby_name game_type")
        str_split = inputCommand.split()
        str_asterisk = str_split[0].split(":")
        self.assertTrue(str_asterisk[0].startswith("*"))
        pass

    def test_check_if_admin(self):
        meApp = MyApp()
        meApp.command = mock.Mock()
        inputCommand = meApp.command("*user:create lobby_name game_type")
        str_split2 = inputCommand.split()
        self.assertEquals(str_split2[0], "*admin")
        pass

    def test_lower(self):
        meApp = MyApp()
        meApp.command = mock.Mock()
        inputCommand = meApp.command("*admin:Create lobby_naMe game_type")
        self.assertTrue(inputCommand.islower())
        pass

    def test_three_arguments(self):
        meApp = MyApp()
        meApp.command = mock.Mock()
        inputCommand = meApp.command("*admin:create lobby_name game_type fourth_command")
        str_command_num = inputCommand.split()
        self.assertEquals(len(str_command_num), 3)
        pass

    def test_lobby_exists(self):
        meApp = MyApp()
        meApp.command = mock.Mock()
        inputCommand = meApp.command("*admin:create lobby_name game_type")
        str_lobby_exists = inputCommand.split()
        self.assertEquals(str_lobby_exists[1], "lobby_name")
        pass

    def test_update_lobby(self):
        meApp = MyApp()
        meApp.command = mock.Mock()
        inputCommand = meApp.command("*admin:update lobby_name game_type")
        str_lobby_exists = inputCommand.split()
        self.assertEqual(inputCommand, "lobby does not exist to update")
        pass

    def test_create_lobby(self):
        meApp = MyApp()
        meApp.command = mock.Mock()
        inputCommand = meApp.command("*admin:create lobby_name game_type")
        self.assertEqual(inputCommand, "lobby does not exist")
        pass
