from django.test import TestCase
from main.apps import MyApp


# user story:
# As a player or admin , I can see the total amount of the pot
# Acceptance Criteria:
# Both admin and player can see the total amount of the pot.
# by typing get_pot, it should return the amount of chips in the pot.
from main.classes import table


class TestPot(TestCase):
    def test_init(self):
        t = table.Table("table_A")
        a = MyApp()
        response = a.doStuff("user1:seepot")
        self.assertEqual(response, "0")

    def test_user_seepot(self):
        t1 = table.Table("table_A")
        t1.pot_size=1000
        a = MyApp()
        response1 = a.doStuff("user1:seepot")
        self.assertEqual(response1, "1000")

        t1.add_pot(1000)
        response2 = a.doStuff("user1:seepot")
        self.assertEqual(response2, "2000")

    def test_admin_seepot(self):
        t2 = table.Table("table_B")
        t2.pot_size = 5000
        a = MyApp()
        response1 = a.doStuff("admin*:seepot")
        self.assertEqual(response1, "5000")

        t2.add_pot(2000)
        response2 = a.doStuff("admin*:seepot")
        self.assertEqual(response2, "7000")
