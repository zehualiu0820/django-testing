from django.test import TestCase
from main.apps import MyApp
from main.classes import Card
from main.classes import table
from main.models import GameTable
from main.models import Person


# user story:
# As a player, I can see the hands of my opponents at the end of a round if they do not fold
# Acceptance Criteria:
# At the end of the game, anyone who didn't fold will have their hands shown
#

class see_opponents_hand(TestCase):
    def show_remaining_hand(self):
        a = MyApp()
        t = table.Table("table1")
        user1 = Person.objects.create(name="user1", highscore=1000)
        user2 = Person.objects.create(name="user2", highscore=2000)
        user3 = Person.objects.create(name="user3", highscore=3000)
        t.add_player(user1)
        t.add_player(user2)
        t.add_player(user3)
        c1 = Card.Card(1, 1)
        c2 = Card.Card(3, 2)
        c3 = Card.Card(5, 3)
        c4 = Card.Card(6, 4)
        c5 = Card.Card(9, 1)
        a.start_game()
        a.arr_Seats[0].hand = [c1, c2, c3, c4, c5]
        a.arr_Seats[1].hand = [c5, c4, c3, c2, c1]
        a.arr_Seats[2].hand = [c1, c3, c5, c2, c4]
        response = a.doStuff("user1:show remaining hand")
        self.assertEqual(response, "Game not end yet!")

        a.doStuff("user1:fold")
        a.doStuff("user2:fold")
        response = a.doStuff("user1:show remaining hand")
        self.assertEqual(response, "AH,5C,9H,3D,6S")

