from django.test import TestCase
from unittest import mock
from main.apps import MyApp


class TestGameType(TestCase):

    def test_game_exists(self):
        meApp = MyApp()
        meApp.command = mock.Mock()
        inputCommand = meApp.command("*admin:create lobby_name game_type")
        self.assertEquals(inputCommand, "lobby does not exist!")
        pass

    def test_update_lobby(self):
        meApp = MyApp()
        meApp.command = mock.Mock()
        inputCommand = meApp.command("*admin:update lobby_name game_type")
        self.assertEqual(inputCommand, "game type does not exist")
        pass
