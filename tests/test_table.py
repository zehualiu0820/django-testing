import unittest
from main.classes import table
from django.test import TestCase
from main.apps import MyApp

# TDD tests


class TestTable(unittest.TestCase):
    t = table.Table("table_A")

    def test_init(self):
        self.assertEqual(self.t.table_name, "table_A")
        pass

    def test_addPlayer(self):
        cur_players = self.t.CUR_NUM_PLAYERS
        add = self.t.add_player()
        self.assertEqual(self.t.CUR_NUM_PLAYERS, (cur_players + 1), "CUR_NUM_PLAYERS is not updating properly")
        self.assertEqual((add, True), "addPlayer() not did not add player")
        pass

    def test_removePlayer(self):
        cur_Players = self.t.CUR_NUM_PLAYERS = 1
        sub = self.t.remove_player()
        self.assertEqual((self.t.CUR_NUM_PLAYERS, (cur_Players - 1)), "CUR_NUM_PLAYERS is not updating properly")
        self.assertEqual((sub, True), "removePlayer() did not remove player"),
        pass

    def test_startGame(self):
        self.t.add_player()
        self.assertNotEqual(self.t.CUR_NUM_PLAYERS, 2)
        self.t.add_player()
        self.assertGreaterEqual(self.t.CUR_NUM_PLAYERS, 2)
        pass

    def test_addPot(self):
        self.t.add_pot(100)
        self.assertEqual(self.t.pot_size, 100)
        self.t.add_pot(-100)
        self.assertEqual(self.t.pot_size, 100)
        pass


# ATDD tests

class ATDD_CreateTable(TestCase):
    def admin_create_table(self):
        t = MyApp()
        response = t.createTable("*admin:create table_name")
        self.assertEqual(response, "table_name has been created")

    def admin_show_tables_populated_seats(self):
        t = MyApp()
        response = t.showTables("*admin:showTables")
        self.assertEqual(response, "tables and players shown")
