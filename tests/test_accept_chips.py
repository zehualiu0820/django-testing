from django.test import TestCase
from main.models import Person, GameTable, Seat
from main.classes.table import Table


class AcceptanceChips(TestCase):
    def set_up(self):
        Person.objects.create(name="user1", highscore=0)
        Person.objects.create(name="user2", highscore=0)
        Person.objects.create(name="user3", highscore=0)

    def starting_chips(self):
        player1 = Person.objects.get(name="user1")
        player2 = Person.objects.get(name="user2")
        table1 = Table()
        table1.add_player(player1)
        table1.add_player(player2)
        table1.start_game()
        self.assertEqual(table1.get_player_chips(player1), 100)
        table1.remove_player(player1)
        table1.remove_player(player2)

    def check_chips(self):
        player1 = Person.objects.get(name="user1")
        player2 = Person.objects.get(name="user2")
        table1 = Table()
        table1.add_player(player1)
        table1.add_player(player2)
        table1.start_game()
        self.assertEqual(table1.get_player_chips, 100)
        table1.set_player_chips(player1, 65)
        self.assertEqual(table1.get_player_chips(player1), 65)
        table1.remove_player(player1)
        table1.remove_player(player2)

    #This combines both "raise more than 5 chips at a time" and "decide how much to raise by"
    def raising(self):
        player1 = Person.objects.get(name="user1")
        player2 = Person.objects.get(name="user2")
        table1 = Table()
        table1.add_player(player1)
        table1.add_player(player2)
        table1.start_game()
        table1.set_player_chips(player1, 4)
        self.assertEqual(table1.raise_pot(player1, 6), "You can't raise more than 5 chips!")
        self.assertEqual(table1.raise_pot(player1, 5), "You can't raise more chips than you have!")
        self.assertEqual(table1.raise_pot(player1, 0), "You can't raise less than 1 chip!")
        self.assertEqual(table1.raise_pot(player1, 4), "You have raised the pot. Current pot: 4 chips,"
                                                       " current raise: 4 chips.")
        table1.remove_player(player1)
        table1.remove_player(player2)

    def calling(self):
        player1 = Person.objects.get(name="user1")
        player2 = Person.objects.get(name="user2")
        player3 = Person.objects.get(name="user3")
        table1 = Table()
        table1.add_player(player1)
        table1.add_player(player2)
        table1.start_game()
        table1.raise_pot(player1, 5)
        self.assertEqual(table1.get_cur_total_raise(player2), "Current Bet is 5 chips, you have 5 left to call.")
        table1.raise_pot(player2, 3)
        self.assertEqual(table1.get_cur_total_raise(player1), "Current Bet is 8 chips, you have 3 left to call.")
        table1.add_pot(player1, 3)
        table1.addplayer(player3)
        table1.start_game()
        table1.raise_pot(player1, 5)
        table1.raise_pot(player2, 3)
        self.assertEqual(table1.get_cur_total_raise(player3), "Current Bet is 8 chips, you have 8 left to call.")
        table1.remove_player(player1)
        table1.remove_player(player2)
        table1.remove_plyaer(player3)