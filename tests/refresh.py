from django.test import TestCase
from main.apps import MyApp
from main.models import Person
from main.models import GameTable
from main.classes import Card
from main.classes import table


class RefreshTest(TestCase):

    def callRefresh(self):
        b = MyApp()
        c1 = Card.Card(1, 1)
        c2 = Card.Card(1, 2)
        c3 = Card.Card(1, 3)
        c4 = Card.Card(1, 4)
        c5 = Card.Card(2, 1)
        c6 = Card.Card(2, 2)
        a = table.Table("test")
        u1 = Person.objects.create(name="U1", highscore=0)
        a.add_player(u1)
        response = b.doStuff("U1:refresh")
        self.assertEqual(response, "Game has not started, only 1 player at the table")
        u2 = Person.objects.create(name="U2", highscore=0)
        a.add_player(u2)
        a.start_game()
        a.dealer = 0
        u1.check()
        response = b.doStuff("U1:refresh")
        self.assertEqual(response, "U1 has checked \n It is U2 turn")
        response = b.doStuff("U1:refresh")
        self.assertEqual(response, "U1 has checked \n It is U2 turn")
        a.arr_Seats[0].hand = [c1, c2, c3, c4, c5]
        a.arr_Seats[1].hand = [c6, c2, c3, c4, c5]
        u2.raises(5)
        response = b.doStuff("U1:refresh")
        self.assertEqual(response, "U2 has raised 5 \n It is U1 turn")
        u1.call()
        response = b.doStuff("U1:refresh")
        self.assertEqual(response, "U1 has called \n Winner is U1 with: [AH, AD, AC, AS, 2H]"
                                   "\n U2 had: [2D, AD, AC, AS, 2H]")

    def joinMidHand(self):
        b = MyApp()
        a = table.Table("test")
        u1 = Person.objects.create(name="U1", highscore=0)
        a.add_player(u1)
        u2 = Person.objects.create(name="U2", highscore=0)
        a.add_player(u2)
        u3 = Person.objects.create(name="U3", highscore=0)
        a.start_game()
        response = b.doStuff("U3:refresh")
        self.assertEqual(response, "Waiting for current hand to finish")

    # test for when a person folds their hand is not shown
    def winning_Test_1(self):
        b = MyApp()
        a = table.Table("test")
        u1 = Person.objects.create(name="U1", highscore=0)
        a.add_player(u1)
        u2 = Person.objects.create(name="U2", highscore=0)
        a.add_player(u2)
        u3 = Person.objects.create(name="U3", highscore=0)
        a.add_player(u3)
        a.start_game()
        c1 = Card.Card(1, 1)
        c2 = Card.Card(1, 2)
        c3 = Card.Card(1, 3)
        c4 = Card.Card(1, 4)
        c5 = Card.Card(2, 1)
        a.arr_Seats[2].hand = [c1, c2, c3, c4, c5]
        u1.fold()
        u2.fold()
        response = b.doStuff("U1:refresh")
        self.assertEqual(response, "U2 has folded \n Winner is U3 with: [AH, AD, AC, AS, 2H]"
                                   "\n U1 had folded \n U2 had folded")
