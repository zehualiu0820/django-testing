from django.test import TestCase
from main.apps import MyApp
from main.classes import Card
from main.classes import table
from main.models import GameTable
from main.models import Person


class SeeHand(TestCase):
    def nonemptyHand(self):
        b = MyApp()
        a = table.Table("test")
        a.name = "testTable"
        u1 = Person.objects.create(name="U1", highscore=0)
        a.add_player(u1)
        u2 = Person.objects.create(name="U2", highscore=0)
        a.add_player(u2)
        a.start_game()
        c1 = Card.Card(1, 1)
        c2 = Card.Card(1, 2)
        c3 = Card.Card(1, 3)
        c4 = Card.Card(1, 4)
        c5 = Card.Card(2, 1)
        a.arr_Seats[0].hand = [c1, c2, c3, c4, c5]
        response = b.doStuff("U1:hand")
        self.assertEqual(response, "AH, AD, AC, AS, 2H")

    def emptyHand(self):
        b = MyApp()
        a = table.Table("test")
        a.name = "testTable"
        u1 = Person.objects.create(name="U1", highscore=0)
        a.add_player(u1)
        u2 = Person.objects.create(name="U2", highscore=0)
        a.add_player(u2)
        a.start_game()
        u1.fold()
        response = b.doStuff("U1:showhand")
        self.assertEqual(response, "empty")
