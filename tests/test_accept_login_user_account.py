from django.test import TestCase
from main.apps import MyApp


# User Story:
#    As a player, I have to log in with an account made by an admin
# Acceptance Criteria:
#   At login, you are able to log into a user only if it exists. If someone is already logged into the
#   specified account, automatically log them out. If an account with specified username does not exist, it spits
#   back an error.


class LoginUserTest(TestCase):
    def login_user_not_logged(self):
        a = MyApp()
        response = a.doStuff("login user")
        self.assertEqual(response, "you are now user")

    def login_user_nonexistent(self):
        a = MyApp()
        response = a.doStuff("login fakeuser")
        self.assertEqual(response, "error: this user does not exist")

    def login_user_already_logged(self):
        a = MyApp()
        response = a.doStuff("login activeuser")
        self.assertEqual(response, "you are now activeuser, active machine logged out")