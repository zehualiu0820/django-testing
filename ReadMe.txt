Admin Commands(*admin):           
addUser <user_name>       -----  creat user with <user_name>
createTable <table_name>  -----  creat table with <table_name>
showTables		          -----  showing all created tables
deleteTable <table_name>  -----  delete table with <table_name>
showScores	              -----  showing user scores
resetScores	              -----  reset user scores


User Commands(user):
showTables		         ------  showing all created tables
joinTable <table_name>   ------  join table with <table_name> 
refresh                  ------  refresh user status on table
showScores		         ------  showing user scores
fold			         ------  discard hand and forfeit interest in the 										    current pot
check			         ------  pass / betting zero
call			         ------  match a bet or match a raise
raise "amount"       	 ------  increase the size of an existing bet in the same betting round. 
seePot                   ------  check the current pot size
leave			         ------  leave the table
showHand <user_name>     ------   At the end of the game, showing hand of <user_name> if he is not folded