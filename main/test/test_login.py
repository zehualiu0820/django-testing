from django.test import TestCase
from main.models import Person


class LoginTestCase(TestCase):
    def setUp(self):
        Person.objects.create(name="testuser", password="1234")

    def test_login_works(self):
        """Ensures that you can only login with the correct parameters"""
        testuser = Person.objects.get(name="testuser")

        self.assertEqual(testuser.verify_login(provided_pass="234"),
                         False)
        self.assertEqual(testuser.verify_login(provided_pass="1234"),
                         True)