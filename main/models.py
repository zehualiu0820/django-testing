from django.db import models

# from main.classes import Seat
from main.classes import Deck
from main.classes import Card


class YourClass:
    def command(self, input_string='placeholder'):
        pass


class GameTable(models.Model):
    name = models.CharField(max_length=10)
    current_num_of_players = models.IntegerField(default=0)
    hand_in_prog = models.BooleanField(default=False)
    cur_dealer = models.IntegerField(default=0)
    recent_bettor = models.IntegerField(default=-1)
    pot_size = models.IntegerField(default=0)
    current_player = models.IntegerField(default=0)
    cur_total_call = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    def find_winner(self):
        # goes through the seats comparing hands and finding the winners, changes the status of the winners to winner
        winner = []
        seats = self.seat.all()
        for p in seats:
            if len(winner) == 0:
                winner.append(p)  # add the first player to the list
            elif p.hand_rank < winner[0].hand_rank:  # if p's hand is a lower rank than the current winners
                winner.clear()  # remove the current winners
                winner.append(p)  # place p in winner
            elif p.hand_rank == winner[0].hand_rank and p.hand_value > winner[0].hand_value:
                # if winner and p have the same kind of hand but p's is a higher value
                winner.clear()  # remove old winners
                winner.append(p)  # put p in winner
            elif p.hand_rank == winner[0].hand_rank and p.hand_value == winner[0].hand_value:
                # if p and winners have the same hand and value add p to winners
                winner.append(p)
        for p in winner:
            p.status = "winner"  # set status of winners to winner
            p.save()
        return len(winner)  # return the number of winners

    def refresh(self):
        x = ""
        winners = 0
        seats = self.seat.all()
        for i in seats:
            if i.status == "winner":
                winners += 1
        if self.current_num_of_players < 2:
            x = "Game has not started, only 1 player at the table"
        elif winners == 0:
            # find previous player
            prev = self.current_player - 1
            if prev < 0:
                prev = self.current_num_of_players - 1
            while seats[prev].status == "not in hand":
                prev -= 1
            if self.recent_bettor < 0:
                x += "Hands has been dealt. It is " + seats[self.current_player].player.name + " turn"
            else:
                x += seats[prev].player.name + " has " + seats[prev].status + "\n It is " + \
                                                                     seats[self.current_player].player.name + " turn"
        else:
            x += seats[self.current_player].player.name + " has " + seats[self.current_player].status + "\n"
            for w in seats:
                if w.status == "winner":
                    x += "Winner is " + w.player.name + " with " + w.showHand() + " "
            for w in seats:
                if w.status != "winner" or w.status != "not in hand":
                    if w.status == "folded":
                        x += w.player.name + " had folded "
                    else:
                        x += w.player.name + " had :" + w.showHand() + " "
        return x

    def deal(self):
        deck = Deck.Deck()
        deck.shuffle()
        for p in self.seat.all():
            p.hand = ""
            c1 = deck.draw()
            c2 = deck.draw()
            c3 = deck.draw()
            c4 = deck.draw()
            c5 = deck.draw()
            p.hand += c1 + ", " + c2 + ", " + c3 + ", " + c4 + ", " + c5
            p.status = "in hand"
            rank = Card.check_rank(p.hand)
            p.hand_rank = rank[0]
            p.hand_value = rank[1]
            p.save()


class Seat(models.Model):
    name = models.CharField(max_length=20)
    table = models.ForeignKey(GameTable, related_name="seat", null=True, on_delete=models.SET_NULL)
    chips = models.IntegerField(default=100)
    status = models.CharField(max_length=20)
    is_dealer = models.BooleanField()
    hand = models.CharField(max_length=18)
    my_chips_in = models.IntegerField(default=0)
    hand_rank = models.IntegerField(default=12)
    hand_value = models.IntegerField(default=0)

    def showHand(self):
        # Show me what you got!
        return self.hand

    def fold(self):
        # Sets the status to folded and pitches hand.
        seats = self.table.seat.all()
        current = self.table.current_player
        if seats[current] != self:
            return "Not your turn yet"
        else:
            self.status = "folded"
            self.hand = ""
            self.save()
            self.table.current_player += 1
            if self.table.current_player > self.table.current_num_of_players - 1:
                self.table.current_player = 0
            if self.table.recent_bettor < 0:
                self.table.recent_bettor = self.table.cur_dealer
            self.table.save()
            return "folded"

    def check_action(self):
        # Checks (hardy har har) to see if there is a bet that still needs to be matched,
        # and if not, essentially passes the turn
        seats = self.table.seat.all()
        current = self.table.current_player
        if seats[current] != self:
            return "Not your turn yet"
        else:
            if self.my_chips_in == self.table.cur_total_call:
                self.status = "checked"
                self.table.current_player += 1
                if self.table.current_player > self.table.current_num_of_players - 1:
                    self.table.current_player = 0
                if self.table.recent_bettor < 0:
                    self.table.recent_bettor = self.table.cur_dealer
                self.save()
                self.table.save()
                return "checked"
            else:
                return "Can't check, there is a bet you have to match!"

    def get_call(self):
        # Finds out how many chips the player must put into the pot at minimum (the current bet)
        my_call = self.table.cur_total_call - self.my_chips_in
        return my_call

    def raise_chips(self, raise_amount=0):
        # Checks for invalid numbers, and checks if the raise + current bet can be matched. If so, a raise is made.
        seats = self.table.seat.all()
        current = self.table.current_player
        if seats[current] != self:
            return "Not your turn yet"
        else:
            if raise_amount < 1:
                error_message = "You can't raise less than 1 chip!"
                return error_message

            elif raise_amount > 5:
                error_message = "You can't raise more than 5 chips!"
                return error_message

            elif raise_amount + self.get_call() > self.chips:
                error_message = "You can't raise when you don't have enough chips to (potentially cover how many chips you" \
                                "need to put in already and) raise!"
                return error_message

            else:
                self.status = "raised"
                self.my_chips_in += raise_amount
                self.chips = self.chips - (raise_amount + self.get_call())
                self.table.cur_total_call += raise_amount
                self.table.pot_size = self.table.pot_size + raise_amount
                self.table.current_player += 1
                if self.table.current_player > self.table.current_num_of_players - 1:
                    self.table.current_player = 0
                if self.table.recent_bettor < 0:
                    self.table.recent_bettor = self.table.cur_dealer
                self.save()
                self.table.save()
                return "Raised the bet by " + str(raise_amount) + " chips!"

    def call_chips(self):
        # Checks if you can match the current bet. If so, you match it. If not, you go all in
        seats = self.table.seat.all()
        current = self.table.current_player
        if seats[current] != self:
            return "Not your turn yet"
        else:
            if self.get_call() > self.chips:
                self.table.pot_size += self.chips
                self.table.save()
                self.chips = 0
                self.status = "all in"
                self.table.current_player += 1
                if self.table.current_player > self.table.current_num_of_players - 1:
                    self.table.current_player = 0
                if self.table.recent_bettor < 0:
                    self.table.recent_bettor = self.table.cur_dealer
                self.save()
                self.table.save()
                return "ALL IN"

            else:
                self.status = "called"
                self.table.pot_size += self.get_call()
                self.table.save()
                self.chips -= self.get_call()
                self.table.current_player += 1
                if self.table.current_player > self.table.current_num_of_players - 1:
                    self.table.current_player = 0
                if self.table.recent_bettor < 0:
                    self.table.recent_bettor = self.table.cur_dealer
                self.save()
                self.table.save()
                return "Called"

    def set_chips(self, new_chips=chips):
        # Allows you to set the amount of chips someone has for the purpose of testing.
        self.chips = new_chips
        self.save()

    def leave_table(self):
        # Automaticaly sets your next action to fold, and then unbinds the seat from the player.
        seats = self.table.seat.all()
        current = self.table.current_player
        if seats[current] != self:
            return "Not your turn yet"
        else:
            self.fold()
            self.chips = 0
            self.is_dealer = False
            self.my_chips_in = 0
            self.player.save()
            self.player = None
            self.save()
            return "You have left the table!"


class Person(models.Model):
    name = models.CharField(max_length=30)
    highscore = models.IntegerField(default=0)
    table = models.CharField(max_length=10, default="")
    seat = models.OneToOneField(Seat, related_name="player", null=True, on_delete=models.SET_NULL)
    password = models.CharField(max_length=12, default="password")
    is_logged_in = models.BooleanField(default=False)

    def verify_login(self, provided_pass=""):
        """
        This is used to check if the password provided for a specific person is the one that matches
        the database. If it is, it checks if is_logged_in is set to true (unimplemented). If it is true,
        then the false is returned. If not, then true is returned. If provided password is not correct,
        false is returned.
        """
        if provided_pass == self.password:
            if self.is_logged_in:
                return False
            else:
                return True
        else:
            return False

    def __str__(self):
        return self.name + "," + str(self.highscore)
