from django.views import View
from django.shortcuts import render, redirect
from .models import Person


class Login(View):
    def get(self, request):
        """
        Wipes any user that's already attached to the session, essentially logging them out.
        Only occurs when directly going to the login page.
        """
        request.session["username"] = ""
        return render(request, "main/login.html", {"message": "Welcome to (totally not) Poker Stars!"})

    def post(self, request):
        """
        Grabs username and password submitted, searches for users in the database. If none are
        found, it prompts the user to register. If found, it runs user.verify_login() to check if
        the password is correct. If it is, no message is set. If it isn't correct, user is prompted
        that it was a bad password. At the end if there is no message (successful login), the active
        session is given the username, and directed to all_tables.html. Otherwise, the page reloads
        with the prompt.
        """
        message = ""
        username = request.POST.get("username", "")
        password = request.POST.get("password", "")
        all_names = list(Person.objects.filter(name=username))
        if len(all_names) == 0:
            message = "Login Failed! Username does not exist, please register!"
        else:
            user = all_names[0]
            if user.verify_login(provided_pass=password):
                pass
            else:
                message = "Login Failed! Bad Password!"
        if message == "":
            request.session["username"] = username
            return redirect("/all_tables.html")
        else:
            return render(request, "main/login.html", {"message": message})
