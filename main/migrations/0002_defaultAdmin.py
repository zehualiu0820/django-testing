from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('main', '0001_initial'),
    ]

    def insert_default_admin(apps, schema_editor):
        administrator = apps.get_model('main', 'Person')
        user = administrator(name="*admin", highscore=0)
        user.save()

    operations = [
        migrations.RunPython(insert_default_admin),
    ]
