# Generated by Django 3.0.5 on 2020-05-01 15:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_defaultAdmin'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='table',
            field=models.CharField(max_length=10, null=True),
        ),
        migrations.CreateModel(
            name='Seat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
                ('chips', models.IntegerField()),
                ('status', models.CharField(max_length=20)),
                ('is_dealer', models.BooleanField()),
                ('hand', models.CharField(max_length=18)),
                ('table', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='seat', to='main.GameTable')),
            ],
        ),
        migrations.AddField(
            model_name='person',
            name='seat',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='player', to='main.Seat'),
        ),
    ]
