from main.classes import Deck
import queue


class Table:
    MAX_PLAYERS = 8
    MIN_PLAYERS = 2
    CUR_NUM_PLAYERS = 0
    arr_Seats = [None] * MAX_PLAYERS
    join_queue = queue.Queue()
    arr_a_winner_is_you = None
    cur_dealer = 0
    recent_bettor = 0
    pot_size = 0
    table_name = None
    is_hand_in_progress = False

    deck = Deck()

    def __init__(self, t_name):
        self.table_name = t_name

    def add_player(self, name):
        # if round in progress, put in queue
        # if round over, pull from queue if not empty OR put name into seat
        if self.is_hand_in_progress:
            self.join_queue.put(name)
            return False
        else:
            if self.CUR_NUM_PLAYERS < self.MAX_PLAYERS:
                self.CUR_NUM_PLAYERS += 1
                for i in self.arr_Seats:
                    if self.arr_Seats[i] is not None:
                        if self.join_queue.empty():
                            self.arr_Seats[i] = name
                        else:
                            self.arr_Seats[i] = self.join_queue.get()
                return True
            return False

    def remove_player(self, name):
        # check if in queue and delete there, if not, then do this:
        q = queue.Queue()
        found = False
        while not self.join_queue.empty():
            tmp = self.join_queue.get()
            if tmp == name:
                found = True
            else:
                q.put(tmp)

        # adds the players back in the order they joined.
        while not q.empty():
            self.join_queue.put(q.get())

        # if name was found in Queue, we can break here without touching the counter of players
        if found:
            return True

        # if name wasn't found in queue, check in current players!
        if self.CUR_NUM_PLAYERS > 0 and self.is_hand_in_progress is False:
            for i in self.arr_Seats:
                if self.arr_Seats[i] == name:
                    self.arr_Seats[i] = None
            self.CUR_NUM_PLAYERS -= 1
            return True
        return False

    def start_game(self):
        if self.CUR_NUM_PLAYERS > 1:
            self.is_hand_in_progress = True
            return True
        return False

    def get_seats(self):
        return self.arr_Seats

    def add_pot(self, fun_bucks):
        self.pot_size += fun_bucks
        return

    # round is over when this is called
    def clear_pot(self):
        tmp = self.pot_size
        self.pot_size = 0
        return tmp

    def change_dealer(self):
        tmp = self.cur_dealer
        while True:
            if self.cur_dealer >= self.MAX_PLAYERS:
                self.cur_dealer = 0
            else:
                self.cur_dealer += 1
            if tmp == self.cur_dealer:
                # if terminates here, something wonky has happened because the same dealer is dealing again!!!
                break
            if self.arr_Seats[self.cur_dealer] is not None:
                # is terminates here, all is well
                break
        return self.cur_dealer

    def change_bettor(self):
        self.recent_bettor += 1
        if self.recent_bettor > self.MAX_PLAYERS:
            self.recent_bettor = 0
        return self.recent_bettor
