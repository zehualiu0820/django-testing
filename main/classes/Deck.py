from main.classes import Card
import random


class Deck:
    def __init__(self):
        # create a standard 52 card deck of cards
        self.deck = []
        self.deckCount = 52
        for s in range(1, 5):
            for r in range(1, 14):
                self.deck.append(Card.Card(s, r))

    def count(self):
        # return the number of cards remaining in the deck
        return self.deckCount

    def draw(self):
        # return and remove the top card in the deck
        # if the deck is empty, raise a ValueError
        if self.deckCount == 0:
            raise ValueError("No cards remaining")
        self.deckCount -= 1
        return self.deck[self.deckCount].__str__()

    def shuffle(self):
        # shuffle the deck using a random number generator
        random.shuffle(self.deck)
