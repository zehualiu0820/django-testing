def rank_to_string(x):
    switch = {
        1: "A",
        2: "2",
        3: "3",
        4: "4",
        5: "5",
        6: "6",
        7: "7",
        8: "8",
        9: "9",
        10: "10",
        11: "J",
        12: "Q",
        13: "K"
    }
    return switch.get(x)


def suit_to_string(x):
    switch = {
        1: "C",
        2: "S",
        3: "H",
        4: "D"
    }
    return switch.get(x)


def check_rank(hand):
    h = hand.split(", ")
    print(len(h))
    cards = []
    ranks = []
    for c in h:
        if len(c) > 2:
            cards.append(c[:2])
            ranks.append(c[2])
        else:
            cards.append(c[0])
            ranks.append(c[1])
    i = 0
    while i <= 4:
        x = cards[i]
        if x == "A":
            cards[i] = 14
        elif x == "J":
            cards[i] = 11
        elif x == "Q":
            cards[i] = 12
        elif x == "K":
            cards[i] = 13
        else:
            cards[i] = int(x)
        i += 1

    cards.sort()

    if cards[0] == 10 and cards[1] == 11 and cards[2] == 12 and cards[3] == 13 and cards[4] == 14:
        if ranks[0] == "H" and ranks[1] == "H" and ranks[2] == "H" and ranks[3] == "H" and ranks[4] == "H":
            return 1, 0
        # royal flush
        elif ranks[0] == "D" and ranks[1] == "D" and ranks[2] == "D" and ranks[3] == "D" and ranks[4] == "D":
            return 1, 0
        # royal flush
        elif ranks[0] == "S" and ranks[1] == "S" and ranks[2] == "S" and ranks[3] == "S" and ranks[4] == "S":
            return 1, 0
        # royal flush
        elif ranks[0] == "C" and ranks[1] == "C" and ranks[2] == "C" and ranks[3] == "C" and ranks[4] == "C":
            return 1, 0
        # royal flush
        else:
            return 6, cards[4]
        # Straight

    elif (cards[1] == cards[0] + 1 and cards[2] == cards[1] + 1 and cards[3] == cards[2] + 1
          and cards[4] == cards[3] + 1) or (cards[0] == cards[4] - 12 and cards[1] == cards[0] + 1
                                            and cards[2] == cards[1] + 1 and cards[3] == cards[2] + 1):
        if ranks[0] == "H" and ranks[1] == "H" and ranks[2] == "H" and ranks[3] == "H" and ranks[4] == "H":
            return 2, cards[4]
        # Straight Flush
        elif ranks[0] == "D" and ranks[1] == "D" and ranks[2] == "D" and ranks[3] == "D" and ranks[4] == "D":
            return 2, cards[4]
        # Straight Flush
        elif ranks[0] == "S" and ranks[1] == "S" and ranks[2] == "S" and ranks[3] == "S" and ranks[4] == "S":
            return 2, cards[4]
        # Straight Flush
        elif ranks[0] == "C" and ranks[1] == "C" and ranks[2] == "C" and ranks[3] == "C" and ranks[4] == "C":
            return 2, cards[4]
        # Straight Flush
        else:
            return 6, cards[4]
        # Straight

    elif cards[1] == cards[0] and cards[2] == cards[0] and cards[3] == cards[0]:
        return 3, cards[0]
        # Four Of A Kind
    elif cards[2] == cards[1] and cards[3] == cards[1] and cards[4] == cards[1]:
        return 3, cards[1]
        # Four Of A Kind

    elif cards[1] == cards[0] and cards[2] == cards[0] and cards[4] == cards[3]:
        return 4, cards[0]
        # Full House
    elif cards[1] == cards[0] and cards[3] == cards[2] and cards[4] == cards[2]:
        return 4, cards[2]
        # Full House
    elif ranks[1] == ranks[0] and ranks[2] == ranks[0] and ranks[3] == ranks[0] and ranks[4] == ranks[0]:
        return 5, cards[4]
        # Flush

    elif cards[1] == cards[0] and cards[2] == cards[0]:
        return 7, cards[0]
        # Three Of a Kind
    elif cards[2] == cards[1] and cards[3] == cards[1]:
        return 7, cards[1]
        # Three Of a Kind
    elif cards[3] == cards[2] and cards[4] == cards[2]:
        return 7, cards[2]
        # Three Of a Kind
    else:
        pair = [] * 2
        single = 1
        for i in range(0, 4):
            if cards[i] == cards[i + 1]:
                pair.append(cards[i])
                single = 0
        if single == 1:
            return 10, cards[4]
        # All single cards
        elif len(pair) == 2:
            return 8, pair[0] + pair[1]
        # two pair
        elif len(pair) == 1:
            return 9, pair[0]
        # one pair


class Card:
    def __init__(self, s, r):
        # initialize a card to the given suit (1-4) and rank (1-13)
        # 1:clubs 2:spades 3:hearts 4:diamonds
        # raise an exception for invalid argument, ValueError
        if s > 4 or s < 1:
            raise ValueError("Suit not in bounds")
        if r > 13 or r < 1:
            raise ValueError("Rank not in bounds")
        self.suit = s
        self.rank = r

    def getSuit(self):
        # return the suit of the card (1-4)
        return self.suit

    def getRank(self):
        # return the rank of the card (1-13)
        return self.rank

    def getValue(self):
        # get the game-specific value of a Card
        # if not used, or game is not defined, always returns 0.
        if self.rank == 1:
            return 14
        return self.rank

    def __str__(self):
        # return rank and suite, as AS for ace of spades, or 3H for
        # three of hearts, JC for jack of clubs.
        s = self.suit
        r = self.rank
        x = ""
        x += rank_to_string(r)
        x += suit_to_string(s)
        return x
