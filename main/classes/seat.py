from .Deck import Deck
from ..models import Seat, Person, GameTable
from django.db import models

class Seat_Object:
    name = ""
    chips = 0
    table = None
    cards = ""
    is_dealer = False
    status = ""
    seat_model = None
    my_chips_in = 0

    def __init__(self):
        self.name = ""
        self.chips = 0
        self.table = None
        self.cards = ""
        self.is_dealer = False
        self.status = ""
        self.seat_model = None
        self.my_chips_in = 0

    def update_class(self,some_seat):
        #Acts as the updater for the class to get the most recent object state.
        self.seat_model = Seat.objects.get(name=some_seat)
        self.chips = self.seat_model.chips
        self.cards = self.seat_model.hand
        self.table = self.seat_model.table
        self.is_dealer = self.seat_model.is_dealer
        self.status = self.seat_model.status
        self.my_chips_in = self.seat_model.my_chips_in

    def update_model(self):
        #Acts as the updater for the model to set object state.
        self.seat_model.chips = self.chips
        self.seat_model.hand = self.cards
        self.seat_model.status = self.status
        self.seat_model.my_chips_in = self.my_chips_in
        self.seat_model.is_dealer = self.is_dealer

    def fold(self):
        #Sets the status to folded and pitches hand.
        self.status = "folded"
        self.cards = ""
        self.update_model()
        models.Model.save(self.seat_model)
        return "folded"

    def check(self):
        #Checks (hardy har har) to see if there is a bet that still needs to be matched, and if not, essentially passes the turn
        if self.my_chips_in == self.table.cur_total_call:
            self.status = "checked"
            self.update_model()
            models.Model.save(self.seat_model)
            return "checked"
        else:
            return "Can't check, there is a bet you have to match!"

    def get_call(self):
        #Finds out how many chips the player must put into the pot at minimum (the current bet)
        my_call = self.table.cur_total_call-self.my_chips_in
        return my_call

    def raise_chips(self, raise_amount=0):
        #Checks for invalid numbers, and checks if the raise + current bet can be matched. If so, a raise is made.
        if raise_amount < 1:
            error_message = "You can't raise less than 1 chip!"
            return error_message

        elif raise_amount > 5:
            error_message = "You can't raise more than 5 chips!"
            return error_message

        elif raise_amount+self.get_call() > self.chips:
            error_message = "You can't raise when you don't have enough chips to (potentially cover how many chips you" \
                            "need to put in already and) raise!"
            return error_message

        else:
            self.chips=self.chips - (raise_amount + self.get_call)
            self.seat_model.table.pot_size=self.seat_model.table.pot_size + raise_amount
            models.Model.save(self.seat_model.table)
            self.update_model()
            models.Model.save(self.seat_model)
            return "Raised the bet by " + str(raise_amount) + " chips!"

    def call_chips(self):
        #Checks if you can match the current bet. If so, you match it. If not, you go all in
        if self.get_call>self.chips:
            self.seat_model.table.pot_size += self.chips
            models.Model.save(self.seat_model.table)
            self.chips = 0
            self.status = "all in"
            self.update_model()
            models.Model.save(self.seat_model)
            return "ALL IN"

        else:
            self.seat_model.table.pot_size += self.get_call
            self.chips -= self.get_call
            self.update_model()
            models.Model.save(self.seat_model)
            return "Called"

    def set_chips(self, new_chips=chips):
        #Allows you to set the amount of chips someone has for the purpose of testing.
        self.chips = new_chips
        self.update_model()
        models.Model.save(self.seat_model)

    def leave_table(self):
        #Automaticaly sets your next action to fold, and then unbinds the seat from the player.
        self.fold(self)
        self.chips = 0
        self.is_dealer = False
        self.my_chips_in = 0
        self.update_model()
        models.Model.save(self.seat_model.player)
        self.seat_model.player = None
        models.Model.save(self.seat_model)
        return "You have left the table!"
