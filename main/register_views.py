from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from main.models import Person
from main.forms.register_form import RegisterForm


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            password = form.cleaned_data['password']
            for per in Person.objects.all():
                if per.name == name:
                    return render(request, 'main/register.html', {'message': "Account already exists", 'form': form})
            user = Person()
            user.name = name
            user.highscore = 0
            user.password = password
            user.save()
            return redirect('main/login.html')
    form = RegisterForm()
    return render(request, 'main/register.html', {'form': form})
