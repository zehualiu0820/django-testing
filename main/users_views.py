from django.shortcuts import render
from main.models import Person, GameTable
from main.forms.users_forms import AddUser


def addUser(request):
    # if "*admin" not in request.session:
    #     message = "Admin privileges needed to access page."
    #     return render(request, "main/login.html", {"name": "", "message": message})

    message = ""
    if request.method == 'GET':
        pass
    if request.method == 'POST':
        form = AddUser(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            cat = form.cleaned_data['cat']
            if cat == "Add":
                bo = createUser(name)
                if bo:
                    return render(request, 'main/User.html', {'message': "User created", 'form': form})
                else:
                    return render(request, 'main/User.html', {'message': "User already exists", 'form': form})
            elif cat == "Delete":
                bo = deleteUser(name)
                if bo:
                    return render(request, 'main/User.html', {'message': "User deleted", 'form': form})
                else:
                    return render(request, 'main/User.html', {'message': "User does not exist", 'form': form})
            elif cat == "Table":
                bo = createTable(name)
                if bo:
                    return render(request, 'main/User.html', {'message': "Table created", 'form': form})
                else:
                    return render(request, 'main/User.html', {'message': "Table already exists", 'form': form})
    form = AddUser()
    message = "<hr><h4>ALL REGISTERED USERS</h4>"
    for p in Person.objects.all():
        message += "<p>" + p.name
        if p.is_logged_in:
            message += ": LOGGED IN"
        message += "</p>"
    message += "<hr>"
    return render(request, 'main/User.html', {'form': form, 'message': message})


def createTable(x):
    table = GameTable()
    for tab in GameTable.objects.all():
        if tab.name == x:
            return False
    table.name = x
    table.save()
    return True


def createUser(x):
    user = Person()
    username = x
    for per in Person.objects.all():
        if per.name == username:
            return False
    user.name = username
    user.highscore = 0
    user.save()
    return True


def deleteUser(x):
    username = x
    for per in Person.objects.all():
        if per.name == username:
            per.delete()
            return True
    return False
