from django.shortcuts import render
from django.views import View
from main.models import Person
from main.models import GameTable
from main.models import Seat
from main.classes.seat import Seat_Object


class Home(View):
    home = 'main/index.html'

    def show_tables(self):
        outTables = ""
        for t in GameTable.objects.all():
            outTables += ("<h4>" + t.__str__() + ":</h4>&emsp;&emsp;&emsp;Players:&emsp;")
            openings1 = 8
            for player in Person.objects.all():
                if player.table.__str__() == t.name:
                    openings1 -= 1
                    outTables += ("\n" + player.name + "&emsp;")
            outTables += ("; Seats Open: " + str(openings1) + "</br>")
        return outTables

    def get(self, request):
        return render(request, self.home, {"user": ""})

    def post(self, request):
        user = Person()
        table = GameTable()
        userName = request.POST["user"]
        commandInput = request.POST["command"]
        active_seat = Seat_Object()

        userName = userName.strip()
        for p in Person.objects.all():
            # Here be Admin commands
            if userName == p.name:
                if '*' in userName:
                    if "addUser" in commandInput:
                        x = commandInput.split(" ")
                        if '*' in x[1]:
                            return render(request, self.home, {"message": "cannot create admin", "user": userName})
                        else:
                            for per in Person.objects.all():
                                if per.name == x[1]:
                                    return render(request, self.home,
                                                  {"message": "Can't add duplicate user ", "user": userName})
                            user.name = x[1]
                            user.highscore = 0
                            user.save()
                            return render(request, self.home, {"message": "user created " + x[1], "user": userName})
                    elif "createTable" in commandInput:
                        x = commandInput.split(" ")
                        table.name = x[1]
                        table.save()
                        response = "table created"
                        return render(request, self.home, {"message": response, "user": userName})
                    elif "showTables" in commandInput:
                        outTables = self.show_tables()
                        return render(request, self.home, {"message": outTables, "user": userName})
                    elif "deleteTable" in commandInput:
                        x = commandInput.split(" ")
                        table.name = x[1]
                        deleted = False
                        for t in GameTable.objects.all():
                            if t.name == table.name:
                                for player in Person.objects.all():
                                    if player.table.__str__() == t.name:
                                        # delete all players at this table
                                        player.table = ""
                                        player.seat = None
                                        player.save()
                                t.delete()
                                deleted = True

                        if deleted:
                            return render(request, self.home, {"message": "table deleted", "user": userName})
                        else:
                            return render(request, self.home, {"message": "table does not exist!", "user": userName})
                    elif "showScores" in commandInput:
                        outString = ""
                        for u in Person.objects.all():
                            if '*' not in u.name:
                                outString += (u.__str__() + "&emsp;&emsp;")
                        return render(request, self.home, {"message": outString, "user": userName})
                    elif "resetScores" in commandInput:
                        for u in Person.objects.all():
                            u.highscore = 0
                            u.save()
                        return render(request, self.home, {"message": "All scores reset", "user": userName})
                    else:
                        return render(request, self.home, {"message": "command not recognized", "user": userName})
                # Here be user commands
                else:
                    if "showTables" in commandInput:
                        outTables = self.show_tables()
                        return render(request, self.home, {"message": outTables, "user": userName})
                    elif "joinTable" in commandInput:
                        x = commandInput.split(" ")
                        tab = ""
                        for t in GameTable.objects.all():
                            if t.name == x[1]:
                                tab = t
                        if p.table != "":
                            response = "already at a table" + p.table
                        elif tab == "":
                            response = "table does not exist"
                        elif tab.current_num_of_players == 4:
                            response = "table is full"
                        else:
                            tab.current_num_of_players += 1
                            seat = Seat()
                            seat.table = tab
                            seat.status = "not in hand"
                            seat.is_dealer = False
                            p.seat = seat
                            p.table = tab.__str__()
                            response = "joined table " + p.table
                            seat.save()
                            tab.save()
                            p.save()
                        if tab.current_num_of_players == 2 and tab.hand_in_prog is False:
                            tab.deal()
                            response += "\n hand dealt"
                            tab.hand_in_prog = True
                            tab.save()
                        return render(request, self.home, {"message": response, "user": userName})
                    elif "refresh" in commandInput:
                        if p.seat.table.current_num_of_players >= 2 and p.seat.table.hand_in_prog is False:
                            p.seat.table.deal()
                        if p.seat.table.current_player == p.seat.table.recent_bettor:
                            num_winner = p.seat.table.find_winner()
                            amt_chip = p.seat.table.pot_size / num_winner
                            tab = p.seat.table
                            for player in tab.seat.all():
                                if player.status == "winner":
                                    player.chips += amt_chip
                        response = p.seat.table.refresh()
                        return render(request, self.home, {"message": response, "user": userName})
                    elif "showScores" in commandInput:
                        outString = ""
                        for u in Person.objects.all():
                            if '*' not in u.name:
                                outString += (u.__str__() + "&emsp;&emsp;")
                        return render(request, self.home, {"message": outString, "user": userName})
                    # The following can only be done is Player is at a Table!!!!
                    # if is_in_a_table
                    elif p.table == "":
                        return render(request, self.home, {"message": "You are not in a table!", "user": userName})
                    elif "fold" in commandInput:
                        response = p.seat.fold()
                        return render(request, self.home, {"message": response, "user": userName})
                    elif "check" in commandInput:
                        response = p.seat.check_action()
                        return render(request, self.home, {"message": response, "user": userName})
                    elif "call" in commandInput:
                        response = p.seat.call_chips()
                        return render(request, self.home, {"message": response, "user": userName})
                    elif "raise" in commandInput:  # index out of range error
                        x = commandInput.split(" ")
                        if len(x) != 2:
                            return render(request, self.home, {"message": "No money has been bet", "user": userName})
                        amount = int(x[1])
                        response = p.seat.raise_chips(raise_amount=amount)
                        return render(request, self.home, {"message": response, "user": userName})

                    elif "seePot" in commandInput:
                        outString = p.seat.table.pot_size
                        return render(request, self.home, {"message": outString, "user": userName})

                    elif "leave" in commandInput:
                        p.highscore += p.seat.chips
                        p.save()
                        p.seat.table.current_num_of_players -= 1
                        p.seat.table.save()
                        response = p.seat.leave_table()
                        return render(request, self.home, {"message": response, "user": userName})

                    elif "showHand" in commandInput:
                        outString = ""
                        if p.seat.table.hand_in_prog is True:
                            if p.seat.status != "folded":
                                outString += (p.seat.showHand() + "\n")
                                return render(request, self.home, {"message": outString, "user": userName})
                        else:
                            return render(request, self.home, {"message": "game not end yet", "user": userName})
                    else:
                        return render(request, self.home, {"message": "command not recognized", "user": userName})
        return render(request, self.home, {"message": "user not doesn't exist", "user": userName})
