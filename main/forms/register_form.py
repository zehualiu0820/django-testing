from django import forms


class RegisterForm(forms.Form):
    name = forms.CharField(label="Username")
    password = forms.CharField(label="Password")
