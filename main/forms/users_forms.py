from django import forms


class AddUser(forms.Form):
    name = forms.CharField(label="Username")
    cat = forms.ChoiceField(choices=[("Add", "Add"), ("Delete", "Delete"), ("Table", "Table")])
