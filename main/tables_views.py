from django.shortcuts import render, redirect
from django.views import View
from main.models import Person
from main.models import GameTable
from main.models import Seat
from main.classes import Card
from main.classes import Deck
from main.classes.seat import Seat_Object


class Table(View):

    def get(self, request):
        username = request.session["username"]
        if not username:
            message = "Login is required! Please login or register!"
            return render(request, "main/table.html", {"name": "", "message": message})
        p1 = 'Player1 empty'
        p2 = 'Player2 empty'
        p3 = 'Player3 empty'
        p4 = 'Player4 empty'
        c1 = 100
        c2 = 100
        c3 = 100
        c4 = 100
        cb1 = 0
        cb2 = 0
        cb3 = 0
        cb4 = 0
        hand1 = 'none'
        hand2 = 'none'
        hand3 = 'none'
        hand4 = 'none'
        players = Seat.objects.all()
        person = Person.objects.all()
        h1 = 0
        h2 = 0
        h3 = 0
        h4 = 0
        for i in players:
            for j in person:
                if i.name == j.name:
                    if i.id == 1:
                        h1 = j.highscore
                    if i.id == 2:
                        h2 = j.highscore
                    if i.id == 3:
                        h3 = j.highscore
                    if i.id == 4:
                        h4 = j.highscore

        for i in players:
            if i.id == 1:
                p1 = i.name
                c1 = i.chips
                cb1 = i.my_chips_in
                hand1 = i.hand
            if i.id == 2:
                p2 = i.name
                c2 = i.chips
                cb2 = i.my_chips_in
                hand2 = i.hand
            if i.id == 3:
                p3 = i.name
                c3 = i.chips
                cb3 = i.my_chips_in
                hand3 = i.hand
            if i.id == 4:
                p4 = i.name
                c4 = i.chips
                cb4 = i.my_chips_in
                hand4 = i.hand
        return render(request, 'main/table.html',
                      {'p1': p1, 'c1': c1, 'cb1': cb1, 'p2': p2, 'c2': c2, 'cb2': cb2, 'p3': p3, 'c3': c3, 'cb3': cb3,
                       'p4': p4, 'c4': c4, 'cb4': cb4, 'hand1': hand1, 'hand2': hand2, 'hand3': hand3, 'hand4': hand4,
                       'h1': h1, 'h2': h2, 'h3': h3, 'h4': h4, 'username': username})

    def leave(self, request):
        if self.method == 'POST':
            players = Seat.objects.all()
            person = Person.objects.all()
            for i in players:
                for j in person:
                    if i.name == j.name:
                        j.highscore += i.chips
                        Person.save()
            return render(request, 'main/table.html', {'leave': self.leave()})
