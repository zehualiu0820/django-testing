from django.views import generic
from main.models import GameTable
from main.models import Person
from django.shortcuts import render


class TableList(generic.ListView):
    context_object_name = 'table_list'
    template_name = 'main/all_tables.html'

    def get_queryset(self):
        return GameTable.objects.order_by('name')

    def get_context_data(self, **kwargs):
        context = super(TableList, self).get_context_data(**kwargs)
        context['score_list'] = Person.objects.order_by('highscore')[:5]
        context['username'] = self.request.session["username"]
        return context

    def get(self, request, *args, **kwargs):
        username = request.session.get('username')
        if not username:
            message = "Login is required! Please login or register!"
            return render(request, "main/all_tables.html", {"name": "", "message": message})
        return super().get(request, *args, **kwargs)
        # render(request, self.template_name, {"username": username})
